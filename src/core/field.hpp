#pragma once

#include <memory>
#include <vector>

#include <gtest/gtest_prod.h>

class Field {
    FRIEND_TEST(TestField, Pos);

    struct Private;
    std::unique_ptr<Private> d_ptr;

public:
    Field();
    Field(const std::size_t _width, const std::size_t _height, const bool fill = false);
    Field(const std::size_t _width, const std::size_t _height, const std::vector<bool> &_data);

    Field(const Field &other);
    Field &operator=(const Field &other);
    Field &operator=(const Field &&other);

    ~Field();

    std::size_t width() const;
    std::size_t height() const;

    void setWidth(const std::size_t width);
    void setHeight(const std::size_t height);

    [[nodiscard]] std::vector<bool>::reference operator ()(std::size_t x, std::size_t y);
    [[nodiscard]] bool operator()(std::size_t x, std::size_t y) const;

    std::size_t neighbours(const std::size_t x, const std::size_t y) const;

    std::size_t count() const;
    std::size_t size() const;

    void fill(const bool filler);
    void clean();

    const std::vector<bool> data() const;

protected:
    std::size_t pos(const std::size_t x, const std::size_t y) const;

}; /// class Field
