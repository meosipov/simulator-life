#include "lifefield.hpp"

#include <memory>
#include <vector>

#include <QDebug>
#include <QPainter>
#include <QRandomGenerator>

#define IMAGE_CELL_OCCUPIED 0xff000000
#define IMAGE_CELL_FREE 0x00ffffff

LifeField::LifeField(QObject *parent) : QObject(parent) {
    auto reset = [this]() {
        m_field.setWidth(this->width());
        m_field.setHeight(this->height());
    };

    connect(this, &LifeField::widthChanged, reset);
    connect(this, &LifeField::heightChanged, reset);

    connect(this, &LifeField::fieldChanged, [this](const Field &field){
        m_population = field.count();
        emit populationChanged(m_population);
    });
}

LifeField::~LifeField() {}

int LifeField::width() const
{
    return m_field.width();
}

int LifeField::height() const
{
    return m_field.height();
}

const Field &LifeField::field() const
{
    return m_field;
}

void LifeField::setField(const Field &field)
{
    m_field = field;
    emit fieldChanged(m_field);
}

// cppcheck-suppress unusedFunction
void LifeField::openImage(const QUrl &url) {
    QImage img(url.toLocalFile());

    img = img.convertToFormat(QImage::Format_Mono);
    img = img.convertToFormat(QImage::Format_RGB32);

    setField(std::move(toField(img)));
}

// cppcheck-suppress unusedFunction
void LifeField::saveImage(const QUrl &url) {
    if (!toImage(m_field).save(url.toLocalFile()))
        qDebug() << "Failed to save file: " << url;
}

void LifeField::setWidth(const int width)
{
    m_field.setWidth(width);
    emit widthChanged(m_field.width());
    emit fieldChanged(m_field);
}

void LifeField::setHeight(const int height)
{
    m_field.setHeight(height);
    emit heightChanged(m_field.height());
    emit fieldChanged(m_field);
}

// cppcheck-suppress unusedFunction
void LifeField::randomize() {
    auto rnd = QRandomGenerator::securelySeeded();
    for (auto i = 0u; i < m_field.width(); ++i)
        for (auto j = 0u; j < m_field.height(); ++j)
            m_field(i, j) = rnd.generate() > 3000000000 ? true : false;

    emit fieldChanged(m_field);
}

void LifeField::clean() {
    m_field.clean();
    emit fieldChanged(m_field);
}

QImage toImage(const Field &field)
{
    QImage img(field.width(), field.height(), QImage::Format_ARGB32);

    for (auto i = 0u; i < field.width(); ++i)
        for (auto j = 0u; j < field.height(); ++j)
            img.setPixel(i, j, field(i, j) ? IMAGE_CELL_OCCUPIED : IMAGE_CELL_FREE);

    return img;
}

Field toField(const QImage &image)
{
    Field field(image.width(), image.height());

    for (auto i = 0; i < image.width(); ++i)
        for (auto j = 0; j < image.height(); ++j)
            field(i, j) = image.pixel(i, j) == IMAGE_CELL_OCCUPIED ? true : false;

    return field;
}

Q_DECLARE_METATYPE(LifeField *)
