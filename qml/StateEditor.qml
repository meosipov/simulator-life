import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2

import me.osipov.life 1.0

Item {
    id: root

    property alias field: display.field

    onFieldChanged: {
        spinWidth.value = field.width;
        spinHeight.value = field.height;
    }

    Action {
        id: actionRandomize
        shortcut: "r"
        onTriggered: {
            console.log("Randomizing")
            root.field.randomize()
        }
    }

    Action {
        id: actionClean
        shortcut: "c"
        onTriggered: {
            console.log("Cleaning")
            root.field.clean()
        }
    }

    Action {
        id: actionOpen
        shortcut: "Ctrl+o"
        onTriggered: {
            console.log("Starging open dialog")
            openDialog.visible = true
        }
    }

    FileDialog {
        id: openDialog
        title: "Select a file"
        folder: shortcuts.home
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]

        onAccepted: {
            console.log("Opening file: " + fileUrl)
            root.field.openImage(fileUrl)
            spinHeight.value = root.field.height
            spinWidth.value = root.field.width
        }

        onRejected: {
            console.log("Canceled")
            visible = false
        }

        visible: false
    }

    RowLayout {
        anchors.fill: parent

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: "black"

            GridEditor {
                id: display
                anchors.fill: parent
            }
        }

        Rectangle {
            Layout.fillHeight: true
            width: 250

            border.width: 0
            color: "#333"

            ColumnLayout {
                anchors.fill: parent

                Text {
                    Layout.fillWidth: true
                    text: qsTr("Field size")
                    color: "white"
                }

                RowLayout {
                    Layout.fillWidth: true
                    Text {
                        Layout.fillWidth: true
                        text: qsTr("Width")
                        color: "white"
                    }

                    SpinBox {
                        id: spinWidth
                        editable: true
                        from: 4
                        to: 1000
                        onValueChanged: {
                            root.field.width = value;
                            focus = false;
                        }
                    }
                }

                RowLayout {
                    Layout.fillWidth: true
                    Text {
                        Layout.fillWidth: true
                        text: qsTr("Height")
                        color: "white"
                    }

                    SpinBox {
                        id: spinHeight
                        editable: true
                        from: 4
                        to: 1000
                        onValueChanged: {
                            root.field.height = value;
                            focus = false;
                        }
                    }
                }

                Button {
                    Layout.fillWidth: true
                    text: qsTr("Randomize")
                    action: actionRandomize
                }

                Button {
                    Layout.fillWidth: true
                    text: qsTr("Clean")
                    action: actionClean
                }

                /// Spacer
                Container {
                    Layout.fillHeight: true
                }
            }
        }
    }
}
