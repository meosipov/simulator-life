#pragma once

#include <string>

class Field;

/**
 * @brief The Calculator class
 */
class Calculator
{
public:
    virtual ~Calculator() {};

    /**
     * @brief name Calculator name
     * @return
     */
    virtual std::string name() const = 0;

    /**
     * @brief step Field step calculation
     * @param field
     * @return
     */
    virtual Field step(const Field &field) const = 0;

}; /// class Calculator
