# Integrating with CI
if(DEFINED ENV{CI_PIPELINE_IID})
    # Using CI id for server builds
    set(PROJECT_VERSION_PATCH $ENV{CI_PIPELINE_IID})
    # Updating PROJECT_VERSION variable to use patch
    set(PROJECT_VERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH})
endif()
