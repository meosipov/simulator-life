#pragma once

#include <optional>

#include <QImage>
#include <QPointer>
#include <QQuickPaintedItem>

/// TODO: Move to fwd declaration if needed
#include "lifefield.hpp"

class QPainter;

class GridDisplay : public QQuickPaintedItem {
    Q_OBJECT

    Q_PROPERTY(LifeField *field MEMBER m_field READ field NOTIFY fieldChanged FINAL)
    Q_PROPERTY(bool heatmap MEMBER m_heatMap NOTIFY heatMapChanged);

public:
    GridDisplay(QQuickItem *parent = nullptr);

    LifeField *field() const { return m_field; }
    void paint(QPainter *painter) override;

    QPoint mapToField(QPoint local) const;

public slots:
    void resetBuffer();
    void clean();

signals:
    void fieldChanged(LifeField *);
    void heatMapChanged(bool);

protected:
    void wheelEvent(QWheelEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    void updateBuffer();
    void updateViewport();

private:
    QPointer<LifeField> m_field;

    QTransform m_contentTransform;
    QRect m_viewportRect;

    QImage m_buffer;
    QImage m_previous;
    QImage m_diffPrevious;
    QImage m_diffNext;
    QImage m_history;

    std::optional<QPointF> m_pressPosition;

    bool m_heatMap = true;
    qreal m_scale = 1.;
}; /// class GridDisplay
