#include "grideditor.hpp"

#include <QPainter>

constexpr auto mouse_click_set_button = Qt::LeftButton;
constexpr auto mouse_click_unset_button = Qt::RightButton;
constexpr auto mouse_click_accept_buttons = mouse_click_set_button | mouse_click_unset_button | Qt::MiddleButton;

GridEditor::GridEditor(QQuickItem *parent) : GridDisplay(parent) {
    setAcceptHoverEvents(true);
    setAcceptedMouseButtons(mouse_click_accept_buttons);
    /// Disabling heatmap in editor
    setProperty("heatmap", false);
}

void GridEditor::mousePressEvent(QMouseEvent *event) {
    updatePosition(event);

    if (event->button() == mouse_click_set_button || event->button() == mouse_click_unset_button) {
        m_pressed = event->button();
        setCell(m_localPos, m_pressed == mouse_click_set_button);
        event->accept();
    } else {
        event->ignore();
        GridDisplay::mousePressEvent(event);
    }
}

void GridEditor::mouseMoveEvent(QMouseEvent *event) {
    updatePosition(event);

    if (m_pressed && (m_pressed == mouse_click_set_button || m_pressed == mouse_click_unset_button)) {
        setCell(m_localPos, m_pressed == mouse_click_set_button);
        event->accept();
    } else {
        event->ignore();
        GridDisplay::mouseMoveEvent(event);
    }
}

void GridEditor::mouseReleaseEvent(QMouseEvent *event) {
    if (m_pressed) {
        m_pressed.reset();
        event->accept();
    } else {
        event->ignore();
        GridDisplay::mouseReleaseEvent(event);
    }
}

bool GridEditor::setCell(const QPoint &pos, bool state)
{
    const auto local = mapToField(pos);
    if (local.isNull())
        return false;

    auto copy = field()->field();
    copy(local.x(), local.y()) = state;
    field()->setField(copy);

    clean();
    return true;
}

void GridEditor::updatePosition(const QMouseEvent *event)
{
    if (!field())
        return;

    m_localPos = event->localPos().toPoint();
}
