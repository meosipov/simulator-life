#include <gtest/gtest.h>

#include "core/field.hpp"

/**
 * @brief The TestField::testing::Test class
 */
class TestField : public ::testing::Test {
protected:
    void SetUp() override {
        field = Field(10u, 10u);
    }

    Field field;

}; /// class TestField

TEST_F(TestField, Creation) {
    EXPECT_EQ(field.width(), 10u);
    EXPECT_EQ(field.height(), 10u);
    EXPECT_EQ(field.size(), 10u * 10u);
}

TEST_F(TestField, Fill) {
    ASSERT_EQ(field.count(), 0u);
    field.fill(true);
    EXPECT_EQ(field.count(), 10u * 10u);
    field.clean();
    EXPECT_EQ(field.count(), 0u);
}

TEST_F(TestField, Equal) {
    field = Field(33u, 47u, true);
    EXPECT_EQ(field.width(), 33u);
    EXPECT_EQ(field.height(), 47u);
    EXPECT_EQ(field.count(), 33u * 47u);
}

TEST_F(TestField, Resize) {
    field.setWidth(20u);
    EXPECT_EQ(field.width(), 20u);
    field.setHeight(30u);
    EXPECT_EQ(field.height(), 30u);
}

TEST_F(TestField, Pos) {
    EXPECT_EQ(field.pos(0u, 0u), 0u);
    EXPECT_EQ(field.pos(4u, 0u), 4u);
    EXPECT_EQ(field.pos(0u, 4u), 40u);
    EXPECT_EQ(field.pos(4u, 4u), 44u);
    EXPECT_THROW(field.pos(10u, 10u), std::runtime_error);
}

TEST_F(TestField, Operator) {
    field = Field(5u, 5u, {
                      false, false, false, false, false,
                      false, true, false, false, false,
                      false, false, false, true, false,
                      false, false, false, false, false,
                      false, false, false, false, false
                  });
    EXPECT_EQ(field.count(), 2u);
    EXPECT_EQ(field(1u, 1u), true);
    EXPECT_EQ(field(3u, 2u), true);
    field(3u, 2u) = false;
    EXPECT_EQ(field.count(), 1u);
    EXPECT_EQ(field(3u, 2u), false);
}

TEST_F(TestField, Neighbours) {
    field = Field(5u, 5u, {
                      false, false, false, false, false,
                      false, true, true, false, true,
                      false, false, false, true, false,
                      false, true, false, false, false,
                      false, false, false, true, false
                  });
    EXPECT_EQ(field.neighbours(2u, 2u), 4u);
    EXPECT_EQ(field.neighbours(0u, 0u), 1u);
    EXPECT_EQ(field.neighbours(4u, 0u), 1u);
    EXPECT_EQ(field.neighbours(0u, 4u), 1u);
    EXPECT_EQ(field.neighbours(4u, 4u), 1u);
}
