#pragma once

#include <optional>

#include "griddisplay.hpp"

class GridEditor : public GridDisplay {
    Q_OBJECT

public:
    GridEditor(QQuickItem *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    bool setCell(const QPoint &pos, bool state = true);
    void updatePosition(const QMouseEvent *event);

private:
    std::optional<Qt::MouseButton> m_pressed;
    QPoint m_localPos;

}; /// class GridEditor
