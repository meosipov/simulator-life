import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.2
import QtGraphicalEffects 1.12
import QtCharts 2.15

import me.osipov.life 1.0

Rectangle {
    id: root

    property LifeField field
    property int speed
    property int speedSteps
    property LifeProcessor processor

    color: "#00000000"

    Action {
        id: actionDiagram
        checkable: true
        checked: false
        shortcut: 'p'
    }

    Rectangle {
        id: statusBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 40

        gradient: Gradient {
            GradientStop { position: 0.0; color: "#cc000000" }
            GradientStop { position: 0.75; color: "#55000000" }
            GradientStop { position: 1.0; color: "#00000000" }
        }

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.bottomMargin: 2
            anchors.topMargin: 2

            Text {
                Layout.preferredWidth: 150
                Layout.fillHeight: true

                verticalAlignment: Text.AlignVCenter
                text: qsTr("Size: ") + (field ? field.width : 0) + "x" + (field ? field.height : 0)
                color: "white"
            }

            Text {
                Layout.preferredWidth: 150
                Layout.fillHeight: true

                verticalAlignment: Text.AlignVCenter
                text: qsTr("Population: ") + (field ? field.population : 0)
                color: "white"
            }

            Text {
                Layout.preferredWidth: 150
                Layout.fillHeight: true

                verticalAlignment: Text.AlignVCenter
                text: qsTr("Iteration: ") + processor.iteration
                color: "white"
            }

            Row {
                Layout.preferredWidth: 150
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft

                spacing: -2

                Repeater {
                    model: root.speed
                    Image {
                        source: "qrc:/img/forward"

                        anchors.verticalCenter: parent.verticalCenter

                        height: parent.height * 0.6
                        fillMode: Image.PreserveAspectFit

                        ColorOverlay {
                            anchors.fill: parent
                            source: parent
                            color: Qt.rgba(1 - root.speed/root.speedSteps, root.speed / root.speedSteps, 0, 1)
                        }
                    }
                }
            }

            Item {
                id: separator
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            Rectangle {
                color: "#00000000"

                Layout.fillHeight: true
                Layout.fillWidth: false
                Layout.preferredWidth: 100

                RowLayout {
                    anchors.fill: parent
                    anchors.margins: 2
                    spacing: 2

                    Image {
                        source: timer.running ? "qrc:/img/progress" : "qrc:/img/paused"
                        fillMode: Image.PreserveAspectFit

                        Layout.fillHeight: true
                        Layout.fillWidth: false
                        Layout.preferredWidth: height
                    }

                    Text {
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        text: timer.running ? "Running" : "Paused"
                        color: timer.running ? "#0f0" : "lightgray"

                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }
                }
            }
        }
    }

    PopulationHistory {
        id: historyModel
        field: root.field
        historyWindow: 100
    }

    ChartView {
        id: populationDiagram
        title: "Population history"

        property int maxPopulation: 0
        property int population: field.population
        onPopulationChanged: maxPopulation = Math.max(population, maxPopulation);

        antialiasing: true
        dropShadowEnabled: true
        backgroundColor: "#88000000"
        theme: ChartView.ChartThemeDark

        height: 200
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        /// Setting up vertical offset and offset animation
        /// Population diagram will flow from the bottom to bottom space
        anchors.verticalCenterOffset: actionDiagram.checked ? (parent.height - height) / 2 : (parent.height + height) / 2
        Behavior on anchors.verticalCenterOffset { PropertyAnimation {} }

        /// Removing legend to save the space
        legend.visible: false

        axes: [
            ValueAxis {
                id: axisIteration
                min: historyModel.historyStart
                max: Math.max(historyModel.historyEnd, historyModel.historyWindow)
            },
            ValueAxis {
                id: axisPopulation
                min: 0 // historyModel.min
                max: historyModel.max
            }
        ]

        AreaSeries {
            name: "Population"

            axisX: axisIteration
            axisY: axisPopulation

            color: "cyan"
            upperSeries: LineSeries {
                /// Using model mapper to get data through QAbstractItemModel from C++
                VXYModelMapper {
                    id: mapper
                    model: historyModel
                    xColumn: 0
                    yColumn: 1
                }
            }
        }
    }
}
