import QtQuick 2.4
import QtQuick.Controls 2.4

import me.osipov.life 1.0

ApplicationWindow {
    id: root
    visible: true

    background: Rectangle {
        color: "black"
    }

    minimumWidth: 800
    minimumHeight: 450

    color: Qt.rgba(0, 0, 0, 0)

    Action {
        id: actionNew
        shortcut: "s"
        onTriggered: {
            state.source = "qrc:/qml/state/StateSimulator";
        }
    }

    Action {
        id: actionEdit
        shortcut: "e"
        onTriggered: {
            state.source = "qrc:/qml/state/StateEditor";
        }
    }

    LifeField {
        id: field
        width: 200
        height: 200
    }

    /// State is used to quick change between application modes
    Loader {
        id: state
        source: "qrc:/qml/state/StateWelcome"
        anchors.fill: parent
        onLoaded: {
            console.log("State changed to " + source);

            /// Passing field if state is available
            if (typeof item.field !== "undefined") {
                item.field = field;
            }
        }
    }

    Action {
        id: actionFullScreen
        checkable: true
        shortcut: "F11"
    }

    Action {
        id: actionEscape
        shortcut: "Esc"
        onTriggered: {
            state.source = "qrc:/qml/state/StateWelcome"
        }
    }

    visibility: actionFullScreen.checked ? "FullScreen" : "Windowed"
}
