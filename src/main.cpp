#include <QIcon>
#include <QApplication>
#include <QQmlApplicationEngine>

#include "gui/griddisplay.hpp"
#include "gui/grideditor.hpp"
#include "gui/lifeprocessor.hpp"
#include "gui/lifefield.hpp"
#include "gui/populationhistorymodel.hpp"

int main(int argc, char **argv) {
    QApplication app(argc, argv);

    app.setWindowIcon(QIcon(":/img/ApplicationIcon"));

    qmlRegisterType<LifeField>("me.osipov.life", 1, 0, "LifeField");
    qmlRegisterType<LifeProcessor>("me.osipov.life", 1, 0, "LifeProcessor");
    qmlRegisterType<GridDisplay>("me.osipov.life", 1, 0, "GridDisplay");
    qmlRegisterType<GridEditor>("me.osipov.life", 1, 0, "GridEditor");
    qmlRegisterType<PopulationHistoryModel>("me.osipov.life", 1, 0, "PopulationHistory");

    QQmlApplicationEngine engine;
    engine.load(QStringLiteral("qrc:/qml/MainWindow"));
    return app.exec();
}
