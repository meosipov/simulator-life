# Cpack macro
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Yet another life simulator")
set(CPACK_PACKAGE_VENDOR "Michael Osipov")
set(CPACK_PACKAGE_CONTACT "osmiev@gmail.com")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "osmiev@gmail.com")
set(CPACK_PACKAGE_ICON images/life.png)

# Version fixes for RPM generator
set(CPACK_PACKAGE_VERSION ${PROJECT_VERSION})

# Using Debian default file naming
set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)

# Autogenerating dependencies information
set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
set(CPACK_RPM_PACKAGE_AUTOREQ ON)

include(CPack)
