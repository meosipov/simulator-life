#include "griddisplay.hpp"

#include <QPainter>

#define MOUSE_DRAG_BUTTON Qt::MiddleButton

GridDisplay::GridDisplay(QQuickItem *parent) : QQuickPaintedItem(parent) {
    setAcceptHoverEvents(true);
    setAcceptedMouseButtons(MOUSE_DRAG_BUTTON);

    connect(this, &GridDisplay::fieldChanged, [this](LifeField *field) {
        resetBuffer();

        /// Using safe disconnectable signals here
        connect(field, SIGNAL(fieldChanged(const Field&)), this, SLOT(resetBuffer()));
    });

    /// Cleaning if heat map property changed
    connect(this, &GridDisplay::heatMapChanged, this, &GridDisplay::clean);
    connect(this, &GridDisplay::widthChanged, this, &GridDisplay::updateViewport);
    connect(this, &GridDisplay::heightChanged, this, &GridDisplay::updateViewport);

    updateViewport();
}

// cppcheck-suppress unusedFunction
void GridDisplay::paint(QPainter *painter) {
    if (m_buffer.isNull())
        updateBuffer();

    /// Applying zoom / offset translations
    painter->setTransform(m_contentTransform);

    /// Drawing background
    painter->fillRect(m_viewportRect, Qt::white);

    /// Alpha blending
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);

    if (m_heatMap) {
        /// Drawing cells where life was
        painter->drawImage(m_viewportRect, m_history);

        /// Drawing previous changed layer
        painter->drawImage(m_viewportRect, m_diffPrevious);
    }

    /// Drawing active cells
    painter->drawImage(m_viewportRect, m_buffer);

    if (m_heatMap) {
        /// Drawing previous changed layer
        painter->drawImage(m_viewportRect, m_diffNext);
    }

    m_previous = m_buffer;
}

QPoint GridDisplay::mapToField(QPoint local) const
{
    /// Mapping to content transformation
    local = m_contentTransform.inverted().map(local);

    /// Skipping if point out of the rect
    if (!m_viewportRect.contains(local))
        return {};

    /// Calculating relative position in rect
    const QPointF relative(1.0 * (local.x() - m_viewportRect.x()) / m_viewportRect.width(),
                           1.0 * (local.y() - m_viewportRect.y()) / m_viewportRect.height());
    /// Calculating field index
    return QPoint(m_field->width() * relative.x(), m_field->height() * relative.y());
}

void GridDisplay::resetBuffer() {
    m_buffer = {};
    update();
}

void GridDisplay::clean()
{
    m_history = {};
    m_previous = {};

    resetBuffer();
}

// cppcheck-suppress unusedFunction
void GridDisplay::wheelEvent(QWheelEvent *event)
{
    /// Updating scale
    m_scale = 1.0 + event->angleDelta().ry() / 20000.0;

    /// Scaling over center
    m_contentTransform.translate(-boundingRect().width() / 2.0 * m_scale, -boundingRect().height() / 2.0 * m_scale);
    m_contentTransform.scale(m_scale, m_scale);
    m_contentTransform.translate(boundingRect().width() / 2.0 / m_scale, boundingRect().height() / 2.0 / m_scale);

    event->accept();
    update();
}

void GridDisplay::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == MOUSE_DRAG_BUTTON) {
        m_pressPosition = event->localPos();
        event->accept();
    } else
        event->ignore();
}

void GridDisplay::mouseMoveEvent(QMouseEvent *event)
{
    if (m_pressPosition) {
        const auto diff = (event->localPos() - *m_pressPosition) * m_scale;
        m_contentTransform.translate(diff.x(), diff.y());
        m_pressPosition = event->localPos();

        update();
        event->accept();
    } else
        event->ignore();
}

void GridDisplay::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == MOUSE_DRAG_BUTTON) {
        m_pressPosition = {};
        event->accept();
    } else
        event->ignore();
}

void GridDisplay::updateBuffer() {
    if (m_field.isNull())
        return;

    m_buffer = toImage(m_field->field());

    if (m_history.isNull()) {
        m_history = QImage(m_buffer.size(), QImage::Format_ARGB32);
        m_history.fill(Qt::white);
    }

    if (m_heatMap) {
        /// Adding to history
        QPainter p(&m_history);
        p.setCompositionMode(QPainter::CompositionMode_Darken);
        p.drawImage(m_history.rect(), m_buffer, m_buffer.rect());
        p.setCompositionMode(QPainter::CompositionMode_Screen);
        p.fillRect(m_diffPrevious.rect(), Qt::cyan);
    }

    if (m_heatMap) {
        /// Highlighting true -> false transitions
        m_diffPrevious = QImage(m_buffer.size(), QImage::Format_ARGB32);
        QPainter p(&m_diffPrevious);
        p.fillRect(m_diffPrevious.rect(), Qt::transparent);
        p.drawImage(m_buffer.rect(), m_buffer);
        p.setCompositionMode(QPainter::CompositionMode_SourceOut);
        p.drawImage(m_previous.rect(), m_previous);
        p.setCompositionMode(QPainter::CompositionMode_SourceIn);
        p.fillRect(m_diffPrevious.rect(), Qt::blue);

        /// Highlighting false -> true transitions
        m_diffNext = QImage(m_buffer.size(), QImage::Format_ARGB32);
        QPainter pp(&m_diffNext);
        pp.fillRect(m_diffNext.rect(), Qt::transparent);
        pp.drawImage(m_previous.rect(), m_previous);
        pp.setCompositionMode(QPainter::CompositionMode_SourceOut);
        pp.drawImage(m_buffer.rect(), m_buffer);
        pp.setCompositionMode(QPainter::CompositionMode_SourceIn);
        pp.fillRect(m_diffNext.rect(), Qt::red);
    }

    updateViewport();
}

void GridDisplay::updateViewport()
{
    /// Moving viewport to the center of screen
    m_viewportRect = m_buffer.rect();

    if (m_viewportRect.isNull())
        return;

    /// Scaling to fit the screen
    const auto windowRatio = width() / height();
    const auto viewportRatio = m_viewportRect.width() / m_viewportRect.height();
    auto scale = 1.0;

    if (windowRatio > viewportRatio)
        scale = height() / m_viewportRect.height();
    else
        scale = width() / m_viewportRect.width();

    /// Scaling viewport
    m_viewportRect.setWidth(m_viewportRect.width() * scale);
    m_viewportRect.setHeight(m_viewportRect.height() * scale);

    /// Placing viewport to the center of screen
    m_viewportRect.moveCenter(boundingRect().center().toPoint());

    update();
}
