#include "field.hpp"

#include <numeric>
#include <stdexcept>

struct Field::Private {
    std::size_t width = 0u, height = 0u;
    std::vector<bool> data;
};

Field::Field()
    : d_ptr(std::make_unique<Field::Private>())
{
}

Field::Field(const std::size_t _width, const std::size_t _height, const bool fill)
    : Field()
{
    d_ptr->width = _width;
    d_ptr->height = _height;

    d_ptr->data.resize(_width * _height);
    std::fill(d_ptr->data.begin(), d_ptr->data.end(), fill);
}

Field::Field(const std::size_t _width, const std::size_t _height, const std::vector<bool> &_data)
    : Field()
{
    d_ptr->width = _width;
    d_ptr->height = _height;
    d_ptr->data = _data;
}

Field::Field(const Field &other)
    : Field()
{
    *this = other;
}

Field &Field::operator=(const Field &other)
{
    d_ptr->data = other.d_ptr->data;
    d_ptr->width = other.d_ptr->width;
    d_ptr->height = other.d_ptr->height;

    return *this;
}

Field &Field::operator=(const Field &&other)
{
    d_ptr->data = std::move(other.d_ptr->data);
    d_ptr->width = other.d_ptr->width;
    d_ptr->height = other.d_ptr->height;

    return *this;
}

Field::~Field()
{
}

std::size_t Field::width() const
{
    return d_ptr->width;
}

std::size_t Field::height() const
{
    return d_ptr->height;
}

void Field::setWidth(const std::size_t _width)
{
    d_ptr->width = _width;
    d_ptr->data.resize(d_ptr->width * d_ptr->height);
}

void Field::setHeight(const std::size_t _height)
{
    d_ptr->height = _height;
    d_ptr->data.resize(d_ptr->width * d_ptr->height);
}

std::vector<bool>::reference Field::operator()(std::size_t x, std::size_t y)
{
    return d_ptr->data[pos(x, y)];
}

std::size_t Field::neighbours(const std::size_t x, const std::size_t y) const
{
    auto count = 0u;

    const auto ix = static_cast<int>(x);
    const auto iy = static_cast<int>(y);

#pragma omp parallel for
    for (int i = ix - 1; i <= ix + 1; ++i) {
        if (i < 0 || static_cast<uint>(i) >= d_ptr->width)
            continue;

        for (int j = iy - 1; j <= iy + 1; ++j) {
            if (j < 0 || static_cast<uint>(j) >= d_ptr->height)
                continue;

            if (ix == i && iy == j)
                continue;

            if (!(*this)(i, j))
                continue;

#pragma omp critical
            ++count;
        }
    }

    return count;
}

std::size_t Field::count() const
{
    return std::accumulate(d_ptr->data.cbegin(), d_ptr->data.cend(), 0u, std::plus<std::size_t>());
}

std::size_t Field::size() const
{
    return d_ptr->data.size();
}

void Field::fill(const bool filler)
{
    std::fill(d_ptr->data.begin(), d_ptr->data.end(), filler);
}

void Field::clean()
{
    fill(false);
}

const std::vector<bool> Field::data() const
{
    return d_ptr->data;
}

bool Field::operator()(std::size_t x, std::size_t y) const
{
    return d_ptr->data[pos(x, y)];
}

std::size_t Field::pos(const std::size_t x, const std::size_t y) const
{
    const auto offset = d_ptr->width * y + x;
    if (offset >= d_ptr->data.size())
        throw std::runtime_error("Invalid position");

    return offset;
}
