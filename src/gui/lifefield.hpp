#pragma once

#include <QUrl>
#include <QImage>
#include <QBitmap>
#include <QObject>
#include <QScopedPointer>
#include <QAbstractTableModel>

#include "core/field.hpp"

QImage toImage(const Field& field);
Field toField(const QImage& img);

class LifeField : public QObject {
    Q_OBJECT

    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(int population MEMBER m_population NOTIFY populationChanged)

public:
    explicit LifeField(QObject *parent = nullptr);
    ~LifeField();

    int width() const;
    int height() const;

    const Field &field() const;

public slots:
    void setField(const Field &field);

    void openImage(const QUrl &url);
    void saveImage(const QUrl &url);

    void setWidth(const int width);
    void setHeight(const int height);

    void randomize();
    void clean();

signals:
    void widthChanged(const int);
    void heightChanged(const int);
    void populationChanged(const int);

    void fieldChanged(const Field &field);

private:
    Field m_field;

    std::size_t m_population{0u};

}; /// class LifeField
