#include "lifeprocessor.hpp"

#include "core/conwaycalculator.hpp"

LifeProcessor::LifeProcessor(QObject *parent) : QObject(parent),
    m_calculator(std::make_unique<ConwayCalculator>()) {
    connect(this, &LifeProcessor::fieldChanged, [this](LifeField *field) {
        m_field = field;
        m_iteration = 0u;
    });
}

void LifeProcessor::step() {
    const auto field = m_calculator->step(m_field->field());
    m_field->setField(std::move(field));

    emit iterationChanged(++m_iteration);
}
