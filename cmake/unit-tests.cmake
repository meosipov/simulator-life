# Unit tests
find_package(GTest REQUIRED)

# This is a test for core part of application. It does not contain Qt dependencies
file(GLOB TEST_SOURCES test/*.cpp)
add_executable(${PROJECT_NAME}-core-test ${TEST_SOURCES} ${SOURCES_CORE})
target_include_directories(${PROJECT_NAME}-core-test PRIVATE ${GTEST_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME}-core-test ${GTEST_LIBRARIES})
enable_testing()
add_test("Core" ${PROJECT_NAME}-core-test)
