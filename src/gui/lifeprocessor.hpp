#pragma once

#include <QObject>
#include <QPointer>

#include "core/calculator.hpp"
#include "lifefield.hpp"

class LifeProcessor : public QObject {
    Q_OBJECT

    Q_PROPERTY(int iteration MEMBER m_iteration NOTIFY iterationChanged FINAL)
    Q_PROPERTY(LifeField *field MEMBER m_field NOTIFY fieldChanged)

public:
    explicit LifeProcessor(QObject *parent = nullptr);

public slots:
    void step();

signals:
    void fieldChanged(LifeField *);
    void iterationChanged(int);

private:
    std::unique_ptr<Calculator> m_calculator;
    int m_iteration{0};
    QPointer<LifeField> m_field;

}; /// class LifeProcessor
