#include "conwaycalculator.hpp"

#include "core/field.hpp"

Field ConwayCalculator::step(const Field &field) const
{
    auto nextstep = field;

#pragma omp parallel for
    for (auto x = 0u; x < field.width(); ++x) {
        for (auto y = 0u; y < field.height(); ++y) {
            const auto neighbours_count = field.neighbours(x, y);
            /// Creating new life
            if (!field(x, y) && neighbours_count == 3u)
#pragma omp critical
                nextstep(x, y) = true;
            /// Killing old life
            else if (field(x, y) && (neighbours_count < 2u || neighbours_count > 3u))
#pragma omp critical
                nextstep(x, y) = false;
        }
    }

    return nextstep;
}
