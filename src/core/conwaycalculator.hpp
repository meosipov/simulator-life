#pragma once

#include "calculator.hpp"

class ConwayCalculator : public Calculator
{
public:
    std::string name() const override { return "Conways Game of Life"; }

    Field step(const Field &field) const override;

}; /// class ConwayCalculator
