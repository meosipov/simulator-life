# Life

## Description
This is a another [Conway`s Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) application.
The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970. It is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves. It is Turing complete and can simulate a universal constructor or any other Turing machine.

Made just for fun.

## Rules
The universe of the Game of Life is an infinite, two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, live or dead, (or populated and unpopulated, respectively). Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:

- Any live cell with fewer than two live neighbours dies, as if by underpopulation.
- Any live cell with two or three live neighbours lives on to the next generation.
- Any live cell with more than three live neighbours dies, as if by overpopulation.
- Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

These rules, which compare the behavior of the automaton to real life, can be condensed into the following:

- Any live cell with two or three live neighbours survives.
- Any dead cell with three live neighbours becomes a live cell.
- All other live cells die in the next generation. Similarly, all other dead cells stay dead.

The initial pattern constitutes the seed of the system. The first generation is created by applying the above rules simultaneously to every cell in the seed, live or dead; births and deaths occur simultaneously, and the discrete moment at which this happens is sometimes called a tick. Each generation is a pure function of the preceding one. The rules continue to be applied repeatedly to create further generations. 

![Color coded racetrack large channel](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Color_coded_racetrack_large_channel.gif/350px-Color_coded_racetrack_large_channel.gif)

## States

Application UI made as state machine. There are 3 states which could be changed via hotkeys

### Welcome
![Welcome screen](readme/welcome.png)

When application starts it launches 'Welcome' state. Welcome screen does not do anything, it's just shows hotkeys list.

### Simulator
![Simulator screen](readme/simulator.gif)

At simulator screen you can view simulation process. Also you can make one of the additional actions:
- Play / pause simulation
- Change simulation speed
- Save field as image

#### Head up display
Simulator has HUD over itself. HUD contains display elements which shows information about simulation process.
Head up display became visible on mouse over event. To show HUD, you need to move over the window.

##### Information bar
Information bar shows basic information about simulation:
- Field size
- Population size
- Speed
- Simulation state: Running / Paused

![Population diagram](readme/hud-info.gif)

##### Population diagram
Population diagram it's a history diagram window which shows population changes process across iterations for last period.

![Population diagram](readme/hud-history.gif)

#### Heatmap
Simulator have a feature named "Heatmap". It can highlight cell activity with different colors.

![Population diagram](readme/simulator-heatmap.gif)

- red: new cells
- black: unchanged cells
- blue: died in previous iteration cells
- cyan: died cells along time ago

### Editor

Editor provide mechanism to modify start state of the field before simulation. It can be easly selected field size. ALso user can randomize start state and clean field.

![Editor screen](readme/editor.gif)

Also, user can draw and undraw cells manually.

![Editor screen](readme/editor-manual.gif)

## Hotkeys
There are several hotkeys through which you can control the application. Some of them will work only in it's own state.

### Global hotkeys
- Esc: Going back to welcome screen
- F11: Maximize / minimize application window
- e: Run editor
- s: Run simulator

### Simulator hotkeys
- Space: Play / pause simulation
- p: Show / hide population diagram
- +: Increase simulation speed
- -: Decrease simulation speed

### Editor hotkeys
- c: Clean field
- r: Randomize field
