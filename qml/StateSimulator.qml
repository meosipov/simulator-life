import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2

import me.osipov.life 1.0
import "qrc:/qml/items"

Item {
    id: root

    property alias field: processor.field

    Action {
        id: actionPlayPause
        checkable: true
        shortcut: "Space"
    }

    LifeProcessor {
        id: processor
    }

    Timer {
        id: timer

        property int steps: 10

        interval: 64
        running: actionPlayPause.checked
        repeat: true
        onTriggered: processor.step()

        onIntervalChanged: {
            console.log("Interval: " + interval)
        }
    }

    Action {
        id: actionHeatMap
        checkable: true
        checked: false
        shortcut: "h"
    }

    Action {
        id: actionSpeedUp
        checkable: false
        shortcut: "+"
        onTriggered: {
            if (timer.interval > 1)
                timer.interval /= 2;
        }
    }

    Action {
        id: actionSpeedDown
        checkable: false
        shortcut: "-"
        onTriggered: {
            if (timer.interval < Math.pow(2, timer.steps))
                timer.interval *= 2;
        }
    }

    Action {
        id: actionSave
        shortcut: "Ctrl+s"
        onTriggered: {
            console.log("Starging save file dialog")
            saveDialog.visible = true
        }
    }

    FileDialog {
        id: saveDialog
        title: "Please select a file or type filename"
        folder: shortcuts.home
        selectExisting: false
        selectFolder: false
        selectMultiple: false
        nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]

        onAccepted: {
            console.log("Saving file: " + fileUrl)
            root.field.saveImage(fileUrl)
        }

        onRejected: {
            console.log("Canceled")
            visible = false
        }

        visible: false
    }

    GridDisplay {
        id: display
        anchors.fill: parent
        field: processor.field
        heatmap: actionHeatMap.checked
    }

    HeadUpDisplay {
        id: hud

        anchors.fill: parent

        field: root.field
        processor: processor
        speed: Math.floor(Math.log(Math.pow(2, timer.steps)/timer.interval)/Math.log(2)) + 1
        speedSteps: timer.steps

        opacity: mouseArea.containsMouse ? 1 : 0
        Behavior on opacity { PropertyAnimation {} }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }
}
