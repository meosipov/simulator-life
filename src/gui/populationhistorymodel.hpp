#pragma once

#include <deque>

#include <QObject>
#include <QPointer>
#include <QQuickItem>
#include <QModelIndex>
#include <QAbstractItemModel>

#include "lifefield.hpp"

class PopulationHistoryModel : public QAbstractItemModel {
    Q_OBJECT

    Q_PROPERTY(LifeField* field READ field WRITE setField NOTIFY fieldChanged)
    Q_PROPERTY(int min READ min NOTIFY minChanged)
    Q_PROPERTY(int max READ max NOTIFY maxChanged)
    Q_PROPERTY(int historyStart READ historyStart NOTIFY historyStartChanged)
    Q_PROPERTY(int historyEnd READ historyEnd NOTIFY historyEndChanged)
    Q_PROPERTY(int historyWindow MEMBER m_historyWindow NOTIFY historyWindowChanged)

public:
    PopulationHistoryModel(QObject* parent = nullptr);

    LifeField* field() const;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    int min() const;
    int max() const;
    int historyStart() const;
    int historyEnd() const;

signals:
    void fieldChanged(LifeField*);
    void minChanged(int);
    void maxChanged(int);
    void historyStartChanged(int);
    void historyEndChanged(int);
    void historyWindowChanged(int);

public slots:
    void setField(LifeField* field);

private slots:
    void updateHistory(int population);
    void updateData();

private:    
    QPointer<LifeField> m_field;
    std::deque<QPoint> m_history;

    int m_min = 0;
    int m_max = 0;
    int m_historyStart = 0;

    int m_historyWindow = 100u;
    int m_iteration = 0u;

}; /// class PopulationHistoryModel

