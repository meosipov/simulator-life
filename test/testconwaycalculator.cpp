#include <gtest/gtest.h>
#include <gtest/gtest-param-test.h>

#include "core/field.hpp"
#include "core/conwaycalculator.hpp"

/// Fix for old version of GTest
#ifndef INSTANTIATE_TEST_SUITE_P
#define INSTANTIATE_TEST_SUITE_P INSTANTIATE_TEST_CASE_P
#endif

/**
 * @brief The TestConwayCalculator::testing::Test class
 */
class TestConwayCalculator: public ::testing::Test {
protected:
    void SetUp() override {
        field = Field(10u, 10u);
    }

    ConwayCalculator calc;
    Field field;

}; /// class TestConwayCalculator

TEST_F(TestConwayCalculator, Empty)
{
    ASSERT_EQ(field.count(), 0u);
    const auto next = calc.step(field);
    EXPECT_EQ(field.data(), next.data());
}

TEST_F(TestConwayCalculator, Full)
{
    ASSERT_EQ(field.size(), 100u);
    field.fill(true);
    ASSERT_EQ(field.count(), field.size());
    const auto next = calc.step(field);
    /// Only on corners
    EXPECT_EQ(next.count(), 4u);
    EXPECT_TRUE(next(0, 0));
    EXPECT_TRUE(next(0, 9));
    EXPECT_TRUE(next(9, 0));
    EXPECT_TRUE(next(9, 9));
}

/**
 * @brief The CalculatorParam struct
 */
struct CalculatorParam {
    /// Request field size
    std::size_t width = 0;
    std::size_t height = 0;
    /// Request field values
    std::vector<bool> request;
    /// Expected is a vector of the calculator next steps results
    /// Using same by default
    std::vector<std::vector<bool>> expected = {request};
}; /// CalculatorParam

/**
 * @brief The TestConwayCalculatorParam::testing::TestWithParam<CalculatorParam> class
 */
class TestConwayCalculatorParam :public ::testing::TestWithParam<CalculatorParam> {
protected:
    ConwayCalculator calc;
    Field field;
}; /// TestConwayCalculatorParam

/// This test is used to check next steps of the calculator
TEST_P(TestConwayCalculatorParam, Figures)
{
    const auto param = GetParam();
    ASSERT_EQ(param.width * param.height, param.request.size());
    ASSERT_FALSE(param.expected.empty());

    field = Field(param.width, param.height, param.request);
    for (const auto &check : param.expected) {
        ASSERT_EQ(check.size(), param.request.size());
        const auto next = calc.step(field);
        EXPECT_EQ(next.data(), check);
        field = std::move(next);
    }
}

/// https://en.wikipedia.org/wiki/Still_life_(cellular_automaton)
INSTANTIATE_TEST_SUITE_P(StillFigures, TestConwayCalculatorParam, ::testing::Values (
                             /// Block
                             CalculatorParam {4, 4, {
                                                  0, 0, 0, 0,
                                                  0, 1, 1, 0,
                                                  0, 1, 1, 0,
                                                  0, 0, 0, 0,
                                              }},
                             /// Bi block
                             CalculatorParam {7, 4, {
                                                  0, 0, 0, 0, 0, 0, 0,
                                                  0, 1, 1, 0, 1, 1, 0,
                                                  0, 1, 1, 0, 1, 1, 0,
                                                  0, 0, 0, 0, 0, 0, 0,
                                              }},
                             /// Hive
                             CalculatorParam {6, 5, {
                                                  0, 0, 0, 0, 0, 0,
                                                  0, 0, 1, 1, 0, 0,
                                                  0, 1, 0, 0, 1, 0,
                                                  0, 0, 1, 1, 0, 0,
                                                  0, 0, 0, 0, 0, 0,
                                              }},
                             /// Loaf
                             CalculatorParam {6, 6, {
                                                  0, 0, 0, 0, 0, 0,
                                                  0, 0, 0, 1, 0, 0,
                                                  0, 0, 1, 0, 1, 0,
                                                  0, 1, 0, 0, 1, 0,
                                                  0, 0, 1, 1, 0, 0,
                                                  0, 0, 0, 0, 0, 0,
                                              }},
                             /// Tub
                             CalculatorParam {5, 5, {
                                                  0, 0, 0, 0, 0,
                                                  0, 0, 1, 0, 0,
                                                  0, 1, 0, 1, 0,
                                                  0, 0, 1, 0, 0,
                                                  0, 0, 0, 0, 0,
                                              }},
                             /// Boat
                             CalculatorParam {5, 5, {
                                                  0, 0, 0, 0, 0,
                                                  0, 1, 1, 0, 0,
                                                  0, 1, 0, 1, 0,
                                                  0, 0, 1, 0, 0,
                                                  0, 0, 0, 0, 0,
                                              }},
                             /// Ship
                             CalculatorParam {5, 5, {
                                                  0, 0, 0, 0, 0,
                                                  0, 1, 1, 0, 0,
                                                  0, 1, 0, 1, 0,
                                                  0, 0, 1, 1, 0,
                                                  0, 0, 0, 0, 0,
                                              }}
                             ));

/// https://www.conwaylife.com/wiki/List_of_common_oscillators
INSTANTIATE_TEST_SUITE_P(OscillatorsFigures, TestConwayCalculatorParam, ::testing::Values (
                             /// Blinker
                             CalculatorParam {3, 3, {
                                                  0, 1, 0,
                                                  0, 1, 0,
                                                  0, 1, 0,
                                              }, {{
                                                      0, 0, 0,
                                                      1, 1, 1,
                                                      0, 0, 0,
                                                  }}},
                             /// Toad
                             CalculatorParam {4, 4, {
                                                  0, 1, 1, 0,
                                                  1, 0, 0, 0,
                                                  0, 0, 0, 1,
                                                  0, 1, 1, 0,
                                              }, {{
                                                      0, 1, 0, 0,
                                                      0, 1, 1, 0,
                                                      0, 1, 1, 0,
                                                      0, 0, 1, 0,
                                                  }}},
                             /// Beacon
                             CalculatorParam {4, 4, {
                                                  1, 1, 0, 0,
                                                  1, 0, 0, 0,
                                                  0, 0, 0, 1,
                                                  0, 0, 1, 1,
                                              }, {{
                                                      1, 1, 0, 0,
                                                      1, 1, 0, 0,
                                                      0, 0, 1, 1,
                                                      0, 0, 1, 1,
                                                  }}},
                             /// Clock
                             CalculatorParam {4, 4, {
                                                  0, 1, 0, 0,
                                                  0, 0, 1, 1,
                                                  1, 1, 0, 0,
                                                  0, 0, 1, 0,
                                              }, {{
                                                      0, 0, 1, 0,
                                                      1, 0, 1, 0,
                                                      0, 1, 0, 1,
                                                      0, 1, 0, 0,
                                                  }}}
                             ));

INSTANTIATE_TEST_SUITE_P(DynamicFigures, TestConwayCalculatorParam, ::testing::Values (
                             /// Glider
                             CalculatorParam {4, 4, {
                                                  0, 1, 0, 0,
                                                  0, 0, 1, 0,
                                                  1, 1, 1, 0,
                                                  0, 0, 0, 0,
                                              }, {{
                                                      0, 0, 0, 0,
                                                      1, 0, 1, 0,
                                                      0, 1, 1, 0,
                                                      0, 1, 0, 0,
                                                  },{
                                                      0, 0, 0, 0,
                                                      0, 0, 1, 0,
                                                      1, 0, 1, 0,
                                                      0, 1, 1, 0,
                                                  },{
                                                      0, 0, 0, 0,
                                                      0, 1, 0, 0,
                                                      0, 0, 1, 1,
                                                      0, 1, 1, 0,
                                                  },{
                                                      0, 0, 0, 0,
                                                      0, 0, 1, 0,
                                                      0, 0, 0, 1,
                                                      0, 1, 1, 1,
                                                  }
                                              }}
                             ));
