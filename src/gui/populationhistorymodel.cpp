#include "populationhistorymodel.hpp"

PopulationHistoryModel::PopulationHistoryModel(QObject *parent)
    : QAbstractItemModel(parent) {
    connect(this, &PopulationHistoryModel::historyWindowChanged, this, &PopulationHistoryModel::updateData);
}

LifeField *PopulationHistoryModel::field() const
{
    return m_field;
}

// cppcheck-suppress unusedFunction
QModelIndex PopulationHistoryModel::index(int row, int column, const QModelIndex &) const
{
    if (row >= static_cast<int>(m_history.size()))
        return QModelIndex();

    if (column >= 2)
        return QModelIndex();

    return createIndex(row, column, nullptr);
}

QModelIndex PopulationHistoryModel::parent(const QModelIndex &) const
{
    return QModelIndex();
}

// cppcheck-suppress unusedFunction
int PopulationHistoryModel::rowCount(const QModelIndex &) const
{
    return m_history.size();
}

// cppcheck-suppress unusedFunction
int PopulationHistoryModel::columnCount(const QModelIndex &) const
{
    return 2;
}

QVariant PopulationHistoryModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        const auto value = m_history.at(index.row());
        return index.column() == 0 ? value.x() : value.y();
    }
    }

    return {};
}

// cppcheck-suppress unusedFunction
int PopulationHistoryModel::min() const
{
    return m_min;
}

int PopulationHistoryModel::max() const
{
    return m_max;
}

// cppcheck-suppress unusedFunction
int PopulationHistoryModel::historyStart() const
{
    if (m_history.empty())
        return {};
    return m_history.front().x();
}

// cppcheck-suppress unusedFunction
int PopulationHistoryModel::historyEnd() const
{
    if (m_history.empty())
        return {};
    return m_history.back().x();
}

void PopulationHistoryModel::setField(LifeField *field)
{
    if (m_field)
        disconnect(m_field.data(), SIGNAL(populationChanged(int)), this, SLOT(updateHistory(int)));

    m_field = field;
    m_history.clear();
    if (m_field)
        m_history.push_back({0, m_field->property("population").toInt()});

    if (m_field)
        connect(m_field.data(), SIGNAL(populationChanged(int)), this, SLOT(updateHistory(int)));

    emit layoutChanged();
}

void PopulationHistoryModel::updateHistory(int population)
{
    ++m_iteration;
    emit historyEndChanged(m_iteration);

    m_history.push_back({m_iteration, population});
    /// Cleaning history to the history window
    while (static_cast<int>(m_history.size()) > m_historyWindow)
        m_history.pop_front();
    emit layoutChanged();

    updateData();
}

void PopulationHistoryModel::updateData()
{
    auto qpoint_compare = [](const QPoint &a, const QPoint &b){ return a.y() < b.y(); };

    const auto max_el = std::max_element(m_history.rbegin(), m_history.rend(), qpoint_compare);
    if (max_el != m_history.rend() && max_el->y() != m_max) {
        m_max = max_el->y();
        maxChanged(m_max);
    }

    const auto min_el = std::min_element(m_history.rbegin(), m_history.rend(), qpoint_compare);
    if (min_el != m_history.rend() && min_el->y() != m_min) {
        m_min = min_el->y();
        minChanged(m_min);
    }

    const auto start = std::max(0, m_iteration - m_historyWindow);
    if (start != m_historyStart) {
        m_historyStart = start;
        emit historyStartChanged(m_historyStart);
    }
}
