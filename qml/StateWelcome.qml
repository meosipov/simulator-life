import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.2

Item {
    id: root

    ColumnLayout {
        anchors.centerIn: parent

        Container {
            /// Spacer
            Layout.fillHeight: true
        }

        Text {
            Layout.alignment: Qt.AlignCenter
            text: "Welcome to life!"
            font.pixelSize: 32
            color: "#fff"
        }

        ToolSeparator {
            Layout.fillWidth: true
            orientation: Qt.Horizontal
        }

        Text {
            id: description
            width: root.width * 0.8
            Layout.alignment: Qt.AlignCenter
            text: "Welcome to yet another Game of Life simulator which was introduced in the 1970 by John Horton Conway.<br>
The game is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input.<br>
One interacts with the Game of Life by creating an initial configuration and observing how it evolves.<br>
It is Turing complete and can simulate a universal constructor or any other Turing machine.<br>
<br>
See <a href='https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life'>https://en.wikipedia.org/wiki/Conway\'s_Game_of_Life</a>"
            textFormat: Text.RichText
            color: "#fff"
            onLinkActivated: Qt.openUrlExternally(link)
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter

            Text {
                Layout.margins: 10
                Layout.alignment: Qt.AlignCenter
                textFormat: Text.RichText
                text: "<b>Hotkeys</b><br>
<br>
<b>Esc</b> - show this screen<br>
<b>F11</b> - toggle fullscreen<br>
<br>
<b>e</b> - run editor<br>
<b>s</b> - run simulator<br>
<b>space</b> - play/pause simulation<br>
<b>h</b> - show/hide heatmap<br>
<b>p</b> - show/hide population diagram
<br>
<b>c</b> - clean field (editor)<br>
<b>r</b> - randomize (editor)"
                color: "#fff"
            }
        }

        Container {
            /// Spacer
            Layout.fillHeight: true
        }
    }

}
